import logo from './logo.svg';
import React from 'react';
import Chat from './Chat'
import './App.css';

function App() {
  return (
    <div className="App">
      <Chat text="Hello" />
    </div>
  );
}

export default App;
