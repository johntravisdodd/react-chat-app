import React from 'react'
import * as signalR from "@microsoft/signalr";

export default class Chat extends React.Component
{
    constructor(props)
    {
        super(props);

        this.textLog = React.createRef();

        // Build SignalR connection
        let connection = new signalR.HubConnectionBuilder()
        .withUrl('/chathub')
        .build();

        this.state =
        {
            ChatConnection: connection,
            ChatMessage: '',
            TextAreaValue: '',
            isLoaded: false
        };

    }

    componentDidMount()
    {
        this.state.ChatConnection.on('ReceiveMessage', (user,message) => {
            const text = this.state.TextAreaValue;
            this.setState({TextAreaValue: text + '\n' + user +': ' + message})
        })

        // Start connection
        this.state.ChatConnection.start()
        .then(() => this.setState({isLoaded: true}))
        .catch(err => console.log('Error while establishing connection: '+err.message));
    }

    render()
    {
        return (
            <div>
                { this.state.isLoaded && 
                <div>
                    <div>
                        Chat Window
                    </div>
                    <textarea value={this.state.TextAreaValue} readOnly={true} />
                    <div>
                        <input type="text" name="ChatMessage" onChange={(input) => {
                            this.setState({ChatMessage: input.target.value});
                        }}/>
                        <input type="submit" value="Send Message" onClick={() => 
                        {
                            this.state.ChatConnection.invoke('SendMessage','TreeBaron',this.state.ChatMessage);
                        }} />
                    </div>
                </div>
                }
            </div>
        )
    }
}
